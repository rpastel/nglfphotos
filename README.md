# README #

I wrote a small AngularJS module to make life easier for developers who are transitioning from server-side storage to client-side storage in the browser. The idea was to make a javaScript domain layer in the spirit of GORM (for those familiar with Grails). I created an angular service that I called 'jdb' that takes a relational database schema definition and exposes domain objects with basic CRUD functions to the user application. The service uses the [Google Lovefield API](https://github.com/google/lovefield) to create and access the database, which could be IndexedDB or WebSQL, depending on the device. 

### View the Demo ###

The demo is a simple photo album application created using AngularJS that lets the user create topics with photos uploaded from their device, search through their topics, and see storage stats. The application depends on the 'jdb' service to create the tables from the schema defined in schema.js and to access the exposed domain objects and CRUD operations. To view the demo:

* Copy the contents of the src folder to the root directory of your favorite webserver and open index.html
* Or, see it running on my school account [here](http://orion.csl.mtu.edu/~mdwoodfo/srdesign/nglfphotos/#/list)

### Documentation ###

An in-depth guide to the internals of the jdb service and the demo photo album application can be found in the doc folder ([here](https://bitbucket.org/rpastel/nglfphotos/src/3c51ed72f6bcd61c1034c8f594cab807760871b6/doc/index.md?at=master&fileviewer=file-view-default)).