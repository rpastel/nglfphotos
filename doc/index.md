# ngLfPhotos Code Documentation
Robert Pastel, 4/7/2016, 4/18/2016
Mark Woodford, 4/20/2016

## Overview
The ngLfPhotos example app demonstrates the use and implementation of a "domain" like ORM for on device browser storage. While most browser data stores are object stores, meaning designed to store JSON objects, most web servers use relational databases, meaning SQL like, to store data. Because the ultimate goal of a typical citizen science application is to collect data and store the data in a centralize database, the difference between object stores and relational databases create challenges for programmers of an offline app, especially inexperience programmers. The challenges are:

* differences in the structure of the data in the data stores,
* language differences in accessing the data in the data stores,
* and moving data from one data store to the other.

Using a data store interface in the browser that is more similar to the server data store interface would eliminate many of the implementation challenges. In addition, student programmers are familiar with using SQL databases and have little experience with object stores, so a SQL browser data store could make offline web app implementation more accessible to the student programmer.
This code documentation first introduces the JDB service, giving a brief description of its use. It then describes the ngLfPhotos AngularJS app. I assume that the reader is familiar with JavaScript and AngularJS:

<https://angularjs.org/>

If not, please work through the AgularJS PhoneCat Tutorial App:

<https://docs.angularjs.org/tutorial>

After the general description of the ngLfPhotos app, this code documentation will give a description of the AngularJS app.js code, describing the controllers' code and the use of the JDB Service in the controllers. Finally, this document will explain the implementation of the JDB service.

## Introduction to jdb Service
The JDB is a "domain" like wrapper for Lovefield.

<https://google.github.io/lovefield/#learn>
<https://github.com/google/lovefield>

Lovefield is a cross browser JavaScript SQL database, which assumes IndexedDB, WebSQL or LocalStorage API on the browser. The JDB wraps around Lovefield API gives the JavaScript programmer a "domain" ORM interface similar to the server side code interface to the database, e.g. the domain classes in Grails. Using Grails' GORM interface, the schema is represented by a collection of domain classes. A table is defined by the domain class that specifies the table columns by the class instance variables. Keywords in the domain classes, such as "has many" and "belongs to", specify the relationships between the domain classes and consequently the tables in the database. A particular row in a table is represented by an instance of the domain class.  The JDB wrapper attempts to imitate this structure, but there are differences between JDB and an ORM domain classes because JavaScript is not a complied language and does not have class definitions. In JDB, the schema is specified by a JSON that specifies all the tables and column names. Only the relation "belongs to" is currently implemented in JDB. A single JSON specification of the schema implies that the programmer cannot design and develop the schema iteratively, but because the browser storage is not intended for persistent storage for more than a few days, the single schema JSON specification is not much of a limitation. And it enables JDB to construct the schema for the developer.

## Schema.js
Schema.js in ngLfPhotos is a module defining two value providers.

<https://docs.angularjs.org/guide/providers>

Schema.js specifies the database name

```js
module.value('databaseName', 'handsAndFingers');
```

and the database schema.

```js
module.value('schema', {
  table1: { 
    column1: {type: 'STRING'},
    column2: {type: 'NUMBER'}
    ...
  },
  table2: { 
    column1: {type: 'INTEGER'},
    column2: {type: 'STRING'}
    ...
  },
  ...
});
```

The schema JSON is a two tiered hierarchical object.  The first tier objects specify the tables in the schema. The keys are table names and the value-object specifies the columns in the table.  In the second tier objects specifying the columns in the table, the keys specify the names of the columns and the value-object defines the data type.

Lovefield has 7 basic data types:

* Array buffer - to store raw bit data
* Boolean
* Date and time
* Integer
* Number
* String
* Object

Although Lovefield uses an enumeration to specify types, JDB uses strings to specify the data types.

JDB data type specification:

* 'ARRAY_BUFFER'
* 'BOOLEAN'
* 'DATE_TIME'
* 'INTEGER'
* 'NUMBER'
* 'STRING'
* 'OBJECT'

JDB adds an additional type which is a reference to another table in the in schema. The reference to another table is also specified by a string, so using JDB, the programmer should not have tables with names of data types, for example there should not be a table named INTEGER.  An example for a schema composed of two tables one representing a 'hand' and the other representing a 'finger' which belongs to a hand is:

```js
schema = {
  hand: {
    side: {type: 'STRING'}
  },
  finger: {
    hand: {type: 'hand'},  // Implicit foreign key
    position: {type: 'STRING'}
  }
};
```

Note that in the schema above 'hand' has a side, 'right' or 'left', and finger has a 'position', 'pointer', 'index', 'middle', or 'pinky' besides belonging to a 'hand'.

The relation of finger 'belonging to' a 'hand' is implemented as a foreign key in the finger table pointing to a 'hand' entry. JDB does not implement join tables. The schema for a typical citizen science app does not require more sophisticated relations. This kind of structure for the data store, I call "bottom up" because children tables (tables that belong to other tables) have references to parent tables. We'll discover that the "bottom up" data store structure is a preferred structure for JS programming.

## Initialization and Database Creation
Creating the database is simplified using JDB. The programmer only needs to inject the schema into the JDB service definition:

```js
var module = angular.module('jdb', ['schema']);
module.service('jdb', function(databaseName, schema) {
  ...
});
```

On initialization, the JDB service will create the database and construct the tables in the schema.

## Table Constructors
Besides creating the data store, the JDB service also adds table constructors to jdb. The table constructors are named by the table name given in the schema. The table constructors can be used with the 'new' keyword to create an instance of a table entry that can later be saved to the data store. An example of creating a 'hand' is:

```js
hand0 = new jdb.hand({side: 'right'});
```

Naturally a 'finger' would require a 'hand':

```js
finger0 = new jdb.finger({hand: hand0, position: 'index'});
```

Table objects inherit basic CRUD operations to manipulate the table and entry. 
In addition, the JDB service adds a 'lf' object for programmers to access the Lovefield methods directly. The 'lf' object consists of two components, a reference to the database object, 'database', and a map-object of the Lovefield table references, 'tables'. Keys to the Lovefield table references are the table names. Because JDB does not implement the select query, the programmer would use 'lf.database' and 'lf.tables' to make the query. For example to select and print to the console all the entries in the 'finger' table:

```js
jdb.lf.database.select()
  .from(jdb.lf.tables.finger)
  .exec()
  .then(function(results) {
    console.log(results);
  });
```

See the Lovefield documentation for more details.

<https://github.com/google/lovefield/blob/master/docs/spec/04_query.md>

## CRUD Operations
Besides the constructor, the JDB service has four CRUD functions associated with the tables.

### get and getAll
The get() function is associated with the table object and is a static method because it does not require an instance of the table object. The get() function can be called either with an id of the requested table row or for convenience an instance of the table row. For example getting a 'hand' instance using the instance is written:

```js
jdb.hand.get(hand0).then(function(result) { ... });
```

While using the id would be called:

```js
var id = 10;
jdb.hand.get(id).then(function(result) { ... });
```

The getAll() functions is also associated with the table object and is a static method. The getAll() function is called without arguments. 

```js
jdb.hand.getAll().then(function (result) { ... });
```

It returns an array table instances, 'results' in the callback example above.

### save and delete
The save() and delete() are methods of the table instances, so they must be invoked on an instance of the table. For example saving a 'hand' instance after constructing it would be written:

```js
hand0.save().then(function() { ... });
```

All of the CRUD functions return promises so any dependent operations of the CRUD calls should be placed in the callback function of the 'then' function of the promise.

## ngLfphotos App
The ngLfphotos app is a photo album. Users can create topics and add photos to the topic. The ngLfphotos app uses Lovefield (ultimately IndexedDB, WebSQL or LocalStorage) to store the topics and photos. This is not an offline app, but it could be made so by using App Cache API to cache the website.

### Template views
The ngLfPhotos app consists of three template views.

* list.html - a list view to show all the existing topics and usage information
* view.html - a view to show all the photos and photo information in a topic
* create.html - a view to make a topic and add photos to the topic.

The three template views are in the ngLfPhotos/ng-views/ directory. The code for the three template views are not complex. The list view uses ng-repeat directive to list the topics from a search. The topic view also uses the ng-repeat directive to list all the photos in a topic. Note the use of the base 64 module to decode the string data in the img-tag.

```html
<img class="img-responsive" ng-src="data:{{photo.filetype}};base64,{{photo.data}}">
```

All of the views make use of a 'busy' scope variable to disable the UI after a call to the database. For example the create view disables the submit button.

```html
<button ng-click="submit()" ng-disabled="busy">Submit</button>
```

There will be more explanation of 'busy' variable, in the controller description. 
All three templates are displayed in the index.html page found at the root directory for the app, ngLPhotos/. The main purpose of index.html is to load all the scripts in the head tag:

```html
<html ng-app="app">
  <head>
    <title>AngularJS LF Photos</title>
    <!-- Include Bootstrap CSS -->
    ...
    <!-- Include JS libraries -->
    <script src="https://ajax.googleapis ... /angularjs/1.4.5/angular.min.js"></script>
    <script src="https://ajax.googleapis ... /angularjs/1.4.7/angular-route.js"></script>
    <script src="https://cdnjs ... /lovefield/2.1.5/lovefield.js"></script>
    <script src="js/angular-base64-upload.js"></script>

    <!-- Include AngularJS modules -->
    <script src="js/schema.js"></script>
    <script src="js/jdb.js"></script>
    <script src="js/app.js"></script>
  </head>

  <body>
    <!-- view template -->
    <div ng-view></div>
  </body>
</html>
```

The JavaStript libraries are:

* AngularJS - single page app framework
  <https://angularjs.org/>
* angular-route.js - is a AngularJS module for routing and associating controllers
  <https://docs.angularjs.org/api/ngRoute>
* lovefield.js - for the data store interface
  <https://github.com/google/lovefield>
* angular-base64-uploads.js - is an AngularJS module for string encoding and file upload
  <https://github.com/adonespitogo/angular-base64-upload>

Modules and services in this app are:

* schema.js - an AngularJS module specifying the database Name and the database schema
* jdb.js - an AngularJS JDB service
* app.js - is the AngularJS module for the app code: controllers etc.

### app.js
The AngularJS code for the ngLfPhoto app are in ngLfPhotos/js/app.js. First the app module is specified.

```js
var module = angular.module('app', ['jdb', 'naif.base64', 'ngRoute']);
```

The app module specifies its dependencies in the injector array. 

* jdb - is the JDB service
* naif.base64 - is the plugin for string encoding the photos and file upload
* ngRougte - is AngularJS service for routing views and controllers.

### Routing
The routing code is at the bottom of the app.js file

```js
module.config(function($routeProvider) {
  $routeProvider

  .when('/list', {
    templateUrl: 'ng-views/list.html',
    controller: 'listTopicsController'
  })

  .when('/topic/:topicId', {
    templateUrl: 'ng-views/view.html',
    controller: 'viewTopicController'
  })

  .when('/create', {
    templateUrl: 'ng-views/create.html',
    controller: 'createTopicController'
  })

  .otherwise({
    redirectTo: '/list'
  });
});
```

The three views are routed to separate controllers. Note the syntax for the topic view. The colon in "/:topicId" defines a parameter in the URL an associates a controller variable, topicId, with the parameter value.

### Controllers

#### listTopicsControllers
The listTopicsController is defined in app.js at

```js
module.controller('listTopicsController', function($scope, jdb) { ... });
```

The controller depends on jdb and $scope. The service jdb is used to interface with the data store and $scope represents the memory for the views. The $scope variable contains all the data required by the view. The callback function in the module.controller function is essentially the constructor for the controller. The arguments in the callback/constructor will automatically be injected by AngularJS into the constructor.

In the listTopicsController, first the busy is set false. This enables the search and clear features on the page. Then the listTopicsController uses JDB getAll() on the topics in the database.

```js
  jdb.topic.getAll().then(function(results) {
    // Please see this site for why scope apply is needed here:
    // http://jimhoskins.com/2012/12/17/angularjs-and-apply.html
    $scope.$apply(function () {
      $scope.topics = results;
      $scope.topicCount = results.length;
    });
  });
```

Note that $scope.$apply(...) is used to make sure that that the data retrieved from the data store is bind with the view. Because JDB/Lovefield change values of $scope behind the scenes (meaning unknown to AngularJS), AngularJS must be told to *digest* the data or apply the changes.

Next ListTopicsController getAll() on photos in the database. This is just to calculate and total number of photos and the total memory usage.

The list view has only one action, clearing or deleting all the topics. The action is registered by assigning a function to the ng-click='clear()' callback:

```js
$scope.clear = function() {
  $scope.busy = true; // Used to prevent multiple requests

  jdb.topic.getAll().then(function(results) {
    var promises = [];
    // Delete each topic. Each delete returns a promise.
    for (var i = 0, len = results.length; i < len; i++) {
      var topic = results[i];
      promises.push(topic.delete());  // Delete also cascades to the topic's photos
    }
    // Wait for all of the deletes operations to complete.
    Promise.all(promises).then(function() {
      $scope.$apply(function () {
        $scope.topics = [];
        $scope.topicCount = 0;
        $scope.photoCount = 0;
        $scope.usage = 0;
        $scope.busy = false;
      });
    });
  });
}
```

Note the $scope.busy variable is set true. This will disable the clear button from operating until it is latter set true at the end of the database operations. The most interesting part of this code is the use of Promises:

<https://www.promisejs.org/>
<https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise>

Promises are new features used in many JS libraries and frameworks. AngularJS use the Q implementation of promises:

<https://docs.angularjs.org/api/ng/service/$q>

Promises are used with asynchronous JS calls and help to eliminate "callback hell".  "Callback hell" is the ever increasing depth of callbacks due to sequential dependencies of the asynchronous calls. When a promise is created it inherits a 'then' method that is called after the fulfillment of the promise. Use of 'then' does not reduce callback depth by itself. But there are two popular design patterns used with promises that do reduce callback depth.

Promises' 'then' can be chained.

<https://www.toptal.com/javascript/javascript-promises>

The structure of sequentially dependent callbacks would be

```js
var promise = New Promise(asyncCall())
promise.then(firstDepenedentCallback (// can have async calls))
  .then(secondDependentCallback())
  .then.(thirdDependentCallback());
```

The second design pattern is based on the use of the Promise.all() method. This technique is used to group asynchronous calls that can be made in parallel but need a single callback after they have all finished.

<https://davidwalsh.name/promises>

The Promise.all method accepts an array of promises, each with their own callback. When all the promises have fulfilled the callback in the Promise.all([...]).then(callback()) is called. So the Promise.all design pattern looks like:

```js
promise1 = new Promise(asyncCall1());
promise2 = new Promise(asyncCall2());
Promise.all([promise1, promise2]).then(finalCallback());
```

In the "clear" handler, after all the topics have been deleted the list view needs to be update with empty list of topics and zero usage statistics. This is an ideal case for the Promise.all method. A promise is made for each topic deletion. They are pushed into an array, promises, that is used as the argument for Promise.all method. The 'then' callback can be used to clear the array of topic names and usage statistics. Note the use of $scope.$apply to update the view.

#### viewTopicController
The viewTopicController is defined in app.js at

```js
module.controller('viewTopicController', function($scope, $routeParams, jdb) { ... });
```

Note that the topic view url has a parameter in the URL, so $routeParams needs to injected into the callback/constructor for the controller in order to get access to the parameter:

```js
var topicId = parseInt($routeParams.topicId, 10);
```

To complete the view, the viewTopicController needs to get the topic from data store

```js
jdb.topic.get(topicId).then(function(result) {
  // Please see this site for why scope apply is needed here:
  // http://jimhoskins.com/2012/12/17/angularjs-and-apply.html
  $scope.$apply(function () {
    $scope.topic = result;
  });
  
  ...
```

After the topic has been retrieved the photos can be retrieved. This requires using database.select() feature. The JDB does not offer  convenient method for selecting from the database, but the database object in jdb offers direct access to Lovefield calls.

```js
...
  jdb.lf.database.select()
    .from(jdb.lf.tables.photo)
    .where(jdb.lf.tables.photo.topic.eq(topicId))
    .exec()
    .then(function(results) {
      $scope.$apply(function () { $scope.photos = results; });
    });
});
```

All Lovefield commands return promises, so the 'then' callback can bind all the photos to the view with $scope.$apply.

#### createTopicController
The createTopicController is defined in app.js at

```js
module.controller('createTopicController', function($scope, $location, jdb) { ... });
```

The createTopicController use $location to redirect page to the list view after the topic has been created.

The only action on the create view is the submit button. The handler is defined at

```js
$scope.submit = function() { ... });
```

First the topic is created from data in the view:

```js
var topic = new jdb.topic({name: $scope.name, desc: $scope.desc, created: new Date()});
```

After the topic is save to the database then all the photos in the view can be stored:

```js
topic.save().then(function() {
  var files = $scope.files;
  var promises = [];
  if (files) {
    // Save all of the photos. Each save returns a promise.
    for (var i = 0, len = $scope.files.length; i < len; i++) {
      var file = $scope.files[i];
      var photo = new jdb.photo({topic: topic, filename: file.filename,
          filetype: file.filetype, filesize: file.filesize, data: file.base64});

      promises.push(photo.save());
    }
  }

  // Wait for all of the save operations to complete.
  Promise.all(promises).then(function() {
    $scope.$apply(function () {
      $location.path('/list');
    });
  });
});
```

Again the Promise.all method is used to wait until all the photos have been stored to redirect to the list view. Perhaps the redirect should have been to the topic view.

## jdb Implementation Details
JDB is an AngularJS module and a service:

```js
var module = angular.module('jdb', ['schema']);
module.service('jdb', function(databaseName, schema) {
  ...
});
```

and only requires the schema and the databaseName.

The callback/constructor uses browser sniffing to determine if the device is iOS.

```js
iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
```

See stakeoverflow for explanation

<http://stackoverflow.com/questions/9038625/detect-if-device-is-ios>

Note that /iPad|iPhone|iPod/ is an regex pattern to test against the browsers navigator.userAgent.

If the device is iOS then WebSQL is used for the data store otherwise the Lovefield's default data store is used which we suspect is IndexedDB. We want to use WebSQL for iOS devices because Apple implemented a faulty IndexedDB in the mobile devices.

The JDB callback/constructor can now create the database and start building the schema using Lovefield's schema builder:

```js
schemaBuilder = lf.schema.create(databaseName, 1);
```

The schema is built by creating tables and adding columns to the tables. So, the callback/constructor iterates through all the first tier objects in the schema JSON, the table names, using

```js
for (var tableName in schema) {
  ...
  // Skip loop iteration if the property is from prototype
  if (!schema.hasOwnProperty(tableName)) { continue; }

  // Skip loop iteration if the table is not an object
  if (typeof(schema[tableName]) != 'object') { continue; }
  ...
}
```

The if-statements make sure that the for-loop iterates only over the table names. The "hasOwnPreperty" method returns only the properties directly attached to the object and not properties inherited by the prototype, so the first skips over any inherited properties. The second if statement does minimal schema structure check.

Next the object for the tableName, the second level tier object, is saved as 'table':

```js
table = schema[tableName];
```

Using tableName, the callback/constructor can create the table and add the column, 'id', which will become the primary key, and the 'id' column is pushed on to array of columns for the lovefield table:

```js
lfTable = schemaBuilder.createTable(tableName);
lfTable.addColumn('id', lf.Type.INTEGER);
columns.push('id');
```

Next the jdb constructor iterates through the second tier of the schema JSON, the column names

```js
for (var columnName in table) {
  var column,
  columnType;

  // Skip loop iteration if the property is from prototype
  if (!table.hasOwnProperty(columnName)) { continue; }

  // Skip loop iteration if the column is not an object
  if (typeof(table[columnName]) != 'object') { continue; }
  ...
}
```

The iterator switches on the type for the column, columnType, weather it is a predefined type or the default which should be a reference to another table. The switch adds the column with the proper type to the Lovefield table. A reference to another table will cascade to the switch's default. The first if-statement checks that the columnName is actually a name of the table defined in the schema. The column type for a reference to another table, i.e. foreign key, should be an integer. Finally it is pushed on to the array of foreign keys:

```js
...
default:
  // If columnType is a string whose name is a table, create a foreign key
  if (schema.hasOwnProperty(columnType)) {
    if (typeof(schema[columnType]) === 'object') {
      lfTable.addColumn(columnName, lf.Type.INTEGER);
      foreignKeys.push({columnName: columnName, columnType: columnType});
    }
  }
...
```

Only after all the columns have been added to the table can the primary key column and foreign key columns be specified in Lovefield.

The primary key column is auto incremented by 

```js
lfTable.addPrimaryKey(['id'], true);
```

All the foreign key columns are iterated through using 

```js
for (var i = 0, len = foreignKeys.length; i < len; i++) {
  ..
}
```

Lovefield requires both a foreign key name and the column name for the foreign key. I believe so that it can construct an auxiliary table to index by the foreign key. 

```js
foreignKey = foreignKeys[i];
fkName = 'fk_' + foreignKey.columnName;
localColumnName = foreignKey.columnName;
refColumnName = foreignKey.columnType + '.id';
                  
lfTable.addForeignKey(fkName, {
  local: localColumnName,
  ref: refColumnName,
  // Delete children rows when the parent row is deleted
  action: lf.ConstraintAction.CASCADE
});
```

Now that schemaBuilder has completed its first major task, constructing the database and the schema, it can now make the constructors for each of the table domains.

```js
for (var tableName in tables) {
  // Skip loop iteration if the property is from prototype
  if (!tables.hasOwnProperty(tableName)) { continue; }

  (function(tableName) {
    // Generic Table Object Constructor
    wrapper[tableName] = function(args) {
      ...
    }

  }(tableName);  // this is tableName from the for-loop
}
```

The anonymous function is immediately called with tableName. This is an example of a loop-body function design pattern. It is used to assure that the context of the loop, the variables within the loops, have the proper associated values. A better practice would be to define the loop-body function outside the loop so that only one function definition is made. In this example of the loop-body function a new function will be defined for each table name.

Note that 'wrapper' in the JDB code is 'this' for the jdb object, so the table constructors are attached to jdb object. First the definition for the table constructor iterates through all the columns for the table and sets them to null.

```js
for (var i = 0, len = tables[tableName].length; i < len; i++) {
  var columnName = tables[tableName][i];
  _this[columnName] = null;
}
```

The table constructor will be called with a JSON specifying the column values. The field names are iterated over and assigned their values.

```js
if (typeof args === 'object' && args !== null) {
  for (var fieldName in args) {
    var fieldValue = null;

    // Skip loop iteration if the property is from prototype
    if (!args.hasOwnProperty(fieldName)) { continue; }

    // Skip loop iteration if the table does not have such a field
    if (!_this.hasOwnProperty(fieldName)) { continue; }

    fieldValue = args[fieldName];
    ...
    // code for reference to another table
    ...
    _this[fieldName] = fieldValue;
    ...
  }
}
```

If the field is a reference to another table then the field value should be an object and the field value should be replaced by the id of the field.

```js
if (typeof(fieldValue) === 'object') {
  if (typeof(fieldValue.id) === 'number') {
    if (fieldValue.id >= 0 && fieldValue.id % 1 === 0) {
      fieldValue = fieldValue.id;
    }
  }
}
```

After the table constructors are defined, the CRUD operations can be defined. The save and delete operations are attached to table objects so they are defined at prototype functions:

```js
wrapper[tableName].prototype.save = function() { ... };
wrapper[tableName].prototype.delete = function() { ... }; 
```

For example the save operation:

```js
wrapper[tableName].prototype.save = function() {
  var _this, fields, table, row, result, promise;

  _this = this;

  return connection.then(function() {
    table = wrapper.lf.tables[tableName];

    fields = {};
    for (var field in _this) {
      if (!_this.hasOwnProperty(field)) { continue; }
      fields[field] = _this[field];
    }

    row = table.createRow(fields);

    result = wrapper.lf.database.insert()
                                .into(table)
                                .values([row])
                                .exec();

    // Update the id of the inserted instance
    promise = result.then(function(inserted) {
        _this.id = inserted[0].id;
    });

    return promise;
  });
};
```

Save and delete table object methods return a promise which is the result from a Lovefield operation. In the case of the save operation the field property and values for the table instance must be gathered and a row created. Then the Lovefield insert command can be executed. The promise from Lovefield, result, can be chained to add the id value to the table object. Finally the promise is returned to the user's code. 

Note that a 'connection' is used to make the Lovefield call. The connection, object, is made later in the JDB code and is attached to the jdb object as a private variable, meaning that it is not immediately exposed to the developer.

The get and getAll functions are functions of the class, like static functions in Java, so they are attached as a property of the table object directly:

```js
wrapper[tableName].get = function(id) { ... };
wrapper[tableName].getAll = function() { ... };
```

For example the getAll method:

```js
wrapper[tableName].getAll = function() {
  var _this,
    table,
    result;

  _this = this;

  return connection.then(function() {
    table = wrapper.lf.tables[tableName];

    result = wrapper.lf.database.select()
          .from(table)
          .exec()
          .then(function(got) {
            var results = [];
            for (var i = 0, len = got.length; i < len; i++) {
              var instance = got[i];
              results.push(new wrapper[tableName](instance));
            }
            return results;
          });

    return result;
  });
};
```

Again the 'connection' object is used to make the select call on the table. Note that a missing 'where' call implies selecting the entire table. All the table rows from the table select call are pushed on to the 'results' array which makes the 'result' for the getAll method. The result is a promise because connection returns a promise. 

Now that all the CRUD operations are defined, the jdb callback/constructor can use schemaBuilder to make the connection object by connecting to the database, get a reference to the Lovefield database and table, and finally attach them to the lf property of jdb:

```js
connection = schemaBuilder.connect(settings).then(function(db) {
    wrapper.lf = {
        database: db,
        tables: {}
    };
            
    for (var tableName in tables) {
        var lfTable;

        // Skip loop iteration if the property is from prototype
        if (!tables.hasOwnProperty(tableName)) { continue; }

        lfTable = wrapper.lf.database.getSchema().table(tableName);
        wrapper.lf.tables[tableName] = lfTable;
    }
});
```
