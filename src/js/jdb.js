(function() {
	var module = angular.module('jdb', ['schema']);

	module.service('jdb', function(databaseName, schema) {
		var iOS,
			iStorageSettings,
			settings,
			schemaBuilder,
			tables,
			wrapper,
			connection;

		wrapper = this;

		// If no database name is provided, initialization must stop
		if (typeof(databaseName) !== 'string') {
			console.error('No database name provided');
			return;
		}

		// If no schema is provided, initialization must stop
		if (typeof(schema) !== 'object') {
			console.error('No schema provided');
			return;
		}
		
		// If using iOS, then use WEB SQL; otherwise use default data store chosen by lovefield
		iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
		iStorageSettings = {
			storeType: lf.schema.DataStoreType.WEB_SQL,
			// Ask for 50 MB. See http://www.html5rocks.com/en/tutorials/offline/quota-research/#toc-safari
			webSqlDbSize: 50*1024*1024
		};
		settings = iOS ? iStorageSettings : {};
		
		// Create database if it doesn't already exist.
		// If a database with the same name but a different schema already exists, lovefield
		// will add the missing columns. If it cannot merge the schemas, then an exception
		// will occur. Currently this is not handled, but can be prevented by clearing the offline
		// storage in the browser for the website of interest. Note that this is not a concern
		// for databases with the same name that belong to different apps (domain separation). 
		schemaBuilder = lf.schema.create(databaseName, 1);
	
		// Build all of the tables defined in the schema
		tables = {};
		for (var tableName in schema) {
			var table,
				lfTable,
				columns,
				foreignKeys;

			// Skip loop iteration if the property is from prototype
			if (!schema.hasOwnProperty(tableName)) { continue; }

			// Skip loop iteration if the table is not an object
			if (typeof(schema[tableName]) != 'object') { continue; }

			table = schema[tableName];
			tables[tableName] = [];
			columns = [];
			foreignKeys = [];

			// Create the table
			lfTable = schemaBuilder.createTable(tableName);

			// Add a column for the id
			lfTable.addColumn('id', lf.Type.INTEGER);
			columns.push('id');

			// Add the columns
			for (var columnName in table) {
				var column,
					columnType;

				// Skip loop iteration if the property is from prototype
				if (!table.hasOwnProperty(columnName)) { continue; }

				// Skip loop iteration if the column is not an object
				if (typeof(table[columnName]) != 'object') { continue; }

				column = table[columnName];
				columnType = column.type;

				switch (columnType) {
					case "ARRAY_BUFFER":
						lfTable.addColumn(columnName, lf.Type.ARRAY_BUFFER);
						break;
					case "BOOLEAN":
						lfTable.addColumn(columnName, lf.Type.BOOLEAN);
						break;
					case "DATE_TIME":
						lfTable.addColumn(columnName, lf.Type.DATE_TIME);
						break;
					case "INTEGER":
						lfTable.addColumn(columnName, lf.Type.INTEGER);
						break;
					case "NUMBER":
						lfTable.addColumn(columnName, lf.Type.NUMBER);
						break;
					case "STRING":
						lfTable.addColumn(columnName, lf.Type.STRING);
						break;
					case "OBJECT":
						lfTable.addColumn(columnName, lf.Type.OBJECT);
						break;
					default:
						// If columnType is a string whose name is a table, create a foreign key
						if (schema.hasOwnProperty(columnType)) {
							if (typeof(schema[columnType]) === 'object') {
								lfTable.addColumn(columnName, lf.Type.INTEGER);
								foreignKeys.push({columnName: columnName, columnType: columnType});
							}
						}
						else {
							console.error("Invalid column type [" + columnType + "]; skipping");
							continue;
						}
				}
				
				columns.push(columnName);
			}

			tables[tableName] = columns;

			// Add the Primary Key (auto-incrementing)
			lfTable.addPrimaryKey(['id'], true);
			
			// Add the Foreign Keys
			for (var i = 0, len = foreignKeys.length; i < len; i++) {
				var foreignKey,
					fkName,
					localColumnName,
					refColumnName;
					
				foreignKey = foreignKeys[i];
				fkName = 'fk_' + foreignKey.columnName;
				localColumnName = foreignKey.columnName;
				refColumnName = foreignKey.columnType + '.id';
					
				lfTable.addForeignKey(fkName, {
					local: localColumnName,
					ref: refColumnName,
					// Delete children rows when the parent row is deleted
					action: lf.ConstraintAction.CASCADE
				});
			}
		}

		// Create constructors for the generic table objects
		for (var tableName in tables) {
			// Skip loop iteration if the property is from prototype
			if (!tables.hasOwnProperty(tableName)) { continue; }

			(function(tableName) {
				// Generic Table Object Constructor
				wrapper[tableName] = function(args) {
					var _this = this;

					// Initialize table fields to null
					for (var i = 0, len = tables[tableName].length; i < len; i++) {
						var columnName = tables[tableName][i];
						_this[columnName] = null;
					}

					// Populate table fields from the arguments object
					if (typeof args === 'object' && args !== null) {
						for (var fieldName in args) {
							var fieldValue = null;

							// Skip loop iteration if the property is from prototype
							if (!args.hasOwnProperty(fieldName)) { continue; }

							// Skip loop iteration if the table does not have such a field
							if (!_this.hasOwnProperty(fieldName)) { continue; }

							fieldValue = args[fieldName];

							// If the argument is an instance of a table, replace the instance with its id
							if (typeof(fieldValue) === 'object') {
								if (typeof(fieldValue.id) === 'number') {
									if (fieldValue.id >= 0 && fieldValue.id % 1 === 0) {
										fieldValue = fieldValue.id;
									}
								}
							}

							_this[fieldName] = fieldValue;
						}
					}
				};
			})(tableName);
		}

		// Create convenience methods for the generic table objects
		for (var tableName in tables) {
			// Skip loop iteration if the property is from prototype
			if (!tables.hasOwnProperty(tableName)) { continue; }

			(function(tableName) {
				// Save the instance
				wrapper[tableName].prototype.save = function() {
					var _this,
						fields,
						table,
						row,
						result,
						promise;

					_this = this;

					return connection.then(function() {
						table = wrapper.lf.tables[tableName];

						fields = {};
						for (var field in _this) {
							if (!_this.hasOwnProperty(field)) { continue; }
							fields[field] = _this[field];
						}

						row = table.createRow(fields);

						result = wrapper.lf.database.insert()
									.into(table)
									.values([row])
									.exec();

						// Update the id of the inserted instance
						promise = result.then(function(inserted) {
							_this.id = inserted[0].id;
						});

						return promise;
					});
				};

				// Delete the instance
				wrapper[tableName].prototype.delete = function() {
					var _this,
						table,
						result;

					_this = this;

					return connection.then(function() {
						table = wrapper.lf.tables[tableName];

						result = wrapper.lf.database.delete()
									.from(table)
									.where(table.id.eq(_this.id))
									.exec();

						return result;
					});
				};

				// Get an instance
				wrapper[tableName].get = function(id) {
					var _this,
						table,
						result;

					_this = this;

					return connection.then(function() {
						table = wrapper.lf.tables[tableName];

						// Support getting an instance when the instance itself is passed in
						if (typeof(id) === 'object') {
							id = id.id;
						}

						// Check that the id is valid
						if (typeof(id) === 'number') {
							if (id >= 0 && id % 1 === 0) {
								result = wrapper.lf.database.select()
											.from(table)
											.where(table.id.eq(id))
											.exec().then(function(got) {
												return new wrapper[tableName](got[0]);
											});
							}
							else {
								console.error('get() id must be an int >= 0');
								result = new Promise(function(resolve, reject) { resolve(null); });
							}
						}
						else {
							console.error('get() id must be an int >= 0');
							result = new Promise(function(resolve, reject) { resolve(null); });
						}

						return result;
					});
				};

				// Get all instances
				wrapper[tableName].getAll = function() {
					var _this,
						table,
						result;

					_this = this;

					return connection.then(function() {
						table = wrapper.lf.tables[tableName];

						result = wrapper.lf.database.select()
									.from(table)
									.exec()
									.then(function(got) {
										var results = [];
										for (var i = 0, len = got.length; i < len; i++) {
											var instance = got[i];
											results.push(new wrapper[tableName](instance));
										}
										return results;
									});

						return result;
					});
				};
			})(tableName);
		}

		// Get a connection to the database
		connection = schemaBuilder.connect(settings).then(function(db) {
			wrapper.lf = {
				database: db,
				tables: {}
			};
			
			for (var tableName in tables) {
				var lfTable;

				// Skip loop iteration if the property is from prototype
				if (!tables.hasOwnProperty(tableName)) { continue; }

				lfTable = wrapper.lf.database.getSchema().table(tableName);
				wrapper.lf.tables[tableName] = lfTable;
			}
		});
	});
})();
