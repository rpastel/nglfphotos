(function() {
	var module = angular.module('app', ['jdb', 'naif.base64', 'ngRoute']);

	// List all topics
	module.controller('listTopicsController', function($scope, jdb) {
		// Inject Math into the view so that we can display the filesize properly
		$scope.Math = window.Math;

		// Used to disable inputs during async operations
		$scope.busy = false;

		// Get all topics
		jdb.topic.getAll().then(function(results) {
			// Please see this site for why scope apply is needed here:
			// http://jimhoskins.com/2012/12/17/angularjs-and-apply.html
			$scope.$apply(function () {
				$scope.topics = results;
				$scope.topicCount = results.length;
			});
		});

		// Calculate usage stats
		jdb.photo.getAll().then(function(results) {
			var usage = 0;
			for (var i = 0, len = results.length; i < len; i++) {
				var photo = results[i];
				usage += photo.filesize;
			}
			$scope.$apply(function () {
				$scope.usage = usage;
				$scope.photoCount = results.length;
			});
		});

		// Handle the clear event - delete all topics and their photos
		$scope.clear = function() {
			$scope.busy = true;	// Used to prevent multiple requests

			jdb.topic.getAll().then(function(results) {
				var promises = [];
				// Delete each topic. Each delete returns a promise.
				for (var i = 0, len = results.length; i < len; i++) {
					var topic = results[i];
					promises.push(topic.delete());	// Delete also cascades to the topic's photos
				}
				// Wait for all of the deletes operations to complete.
				Promise.all(promises).then(function() {
					$scope.$apply(function () {
						$scope.topics = [];
						$scope.topicCount = 0;
						$scope.photoCount = 0;
						$scope.usage = 0;
						$scope.busy = false;
					});
				});
			});
		};
	});

	// View a topic
	module.controller('viewTopicController', function($scope, $routeParams, jdb) {
		// Inject Math into the view so that we can display the filesize properly
		$scope.Math = window.Math;

		var topicId = parseInt($routeParams.topicId, 10);

		// Get the topic
		jdb.topic.get(topicId).then(function(result) {
			// Please see this site for why scope apply is needed here:
			// http://jimhoskins.com/2012/12/17/angularjs-and-apply.html
			$scope.$apply(function () {
				$scope.topic = result;
			});
			// Get all of the photos belonging to the topic.
			// Do this query using the lovefield api
			jdb.lf.database.select()
				.from(jdb.lf.tables.photo)
				.where(jdb.lf.tables.photo.topic.eq(topicId))
				.exec()
				.then(function(results) {
					$scope.$apply(function () {
						$scope.photos = results;
					});
				});
		});
	});

	// Create a topic with photos
	module.controller('createTopicController', function($scope, $location, jdb) {
		// Used to disable inputs during async operations
		$scope.busy = false;

		// Handle the submit event - save the topic and its photos
		$scope.submit = function() {
			$scope.busy = true;	// Prevent multiple requests

			var topic = new jdb.topic({name: $scope.name, desc: $scope.desc, created: new Date()});

			// Save the topic
			topic.save().then(function() {
				var files = $scope.files;
				var promises = [];
				if (files) {
					// Save all of the photos. Each save returns a promise.
					for (var i = 0, len = $scope.files.length; i < len; i++) {
						var file = $scope.files[i];

						var photo = new jdb.photo({topic: topic, filename: file.filename,
								filetype: file.filetype, filesize: file.filesize, data: file.base64});

						promises.push(photo.save());
					}
				}

				// Wait for all of the save operations to complete.
				Promise.all(promises).then(function() {
					$scope.$apply(function () {
						$location.path('/list');
					});
				});
			});
		};
	});

	// Configure the routes
	module.config(function($routeProvider) {
		$routeProvider

		.when('/list', {
			templateUrl: 'ng-views/list.html',
			controller: 'listTopicsController'
		})

		.when('/topic/:topicId', {
			templateUrl: 'ng-views/view.html',
			controller: 'viewTopicController'
		})

		.when('/create', {
			templateUrl: 'ng-views/create.html',
			controller: 'createTopicController'
		})

		.otherwise({
			redirectTo: '/list'
		});
	});
})();
