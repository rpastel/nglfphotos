(function() {
	var module = angular.module('schema', []);

	module.value('databaseName', 'ngLfPhotos');

	module.value('schema', 	{
		topic: {
			name: {type: 'STRING'},
			desc: {type: 'STRING'},
			created: {type: 'DATE_TIME'}
		},
		photo: {
			topic: {type: 'topic'},	// Implicit foreign key
			filename: {type: 'STRING'},
			filetype: {type: 'STRING'},
			filesize: {type: 'INTEGER'},
			data: {type: 'STRING'}	// Base-64 encoded string
		} 
	});
})();
